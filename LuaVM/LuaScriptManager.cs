﻿using System;
using System.Linq;
using System.Collections.Generic;
using MoonSharp.Interpreter;
using System.Diagnostics;
using MoonSharp.Interpreter.Loaders;
using aitest.Scripting.VM;
using System.IO;

namespace aitest.LuaVM
{
	public sealed class LuaScriptManager : ILuaScriptManager
	{
		private readonly ICoroutineVM _vm;

		public LuaScriptManager(ICoroutineVM vm)
		{
			_vm = vm;
		}

		public ILuaScript LoadScript(string filename, IDictionary<string, object> globals, params object[] args)
		{
			Script script;
			Closure entrypoint;

			try
			{
				TryInternalLoadScript (filename, globals, out script, out entrypoint);

				entrypoint.Call (args);
			}
			catch(InterpreterException e)
			{
				Console.WriteLine (e.DecoratedMessage);
				return null;
			}

			return new LuaScript(script, null);
		}

		public ILuaScript LoadScript(string filename, object globals, params object[] args)
		{
			return LoadScript (filename, AnonymousObjectToDict(globals), args);
		}

		public ILuaScript LoadScript(string filename)
		{
			return this.LoadScript (filename, null);
		}

		public ILuaScript PreloadScript(string filename, IDictionary<string, object> globals)
		{
			Script script;
			Closure entrypoint;
			if (TryInternalLoadScript (filename, globals, out script, out entrypoint))
			{
				return new LuaScript (script, entrypoint);
			}
			return null;
		}

		public ILuaScript PreloadScript(string filename, object globals)
		{
			return PreloadScript (filename, AnonymousObjectToDict (globals));
		}

		public ILuaScript PreloadScript(string filename)
		{
			return PreloadScript (filename, null);
		}

		public ILuaCoroutine ExecVM(string filename, object globals, params object[] args)
		{
			var script = this.PreloadScript (filename, globals);
			var coroutine = script.CreateEntrypointCoroutine (args);
			_vm.AddCoroutine (coroutine as IRunnableCoroutine);
			return coroutine;
		}

		public void ExecCoroutine(ILuaCoroutine coroutine)
		{
			_vm.AddCoroutine (coroutine as IRunnableCoroutine);
		}

		private bool TryInternalLoadScript(string filename, IDictionary<string, object> globals, out Script script, out Closure entrypoint)
		{
			script = new Script (CoreModules.Preset_SoftSandbox | CoreModules.LoadMethods);
			script.Options.ScriptLoader = new PackageScriptLoader ();

			script.Options.DebugPrint = x => Console.WriteLine ("DEBUG: " + x);
			script.Options.DebugInput = x => string.Empty;
			script.Options.Stdout = Stream.Null;
			script.Options.Stderr = Stream.Null;
			script.Options.Stdin = Stream.Null;

			var loadRet = script.LoadFile (filename);

			if (globals != null)
			{
				foreach (var item in globals)
				{
					UserData.RegisterType (item.Value.GetType (), InteropAccessMode.LazyOptimized);
					script.Globals [item.Key] = item.Value;
				}
			}

			entrypoint = loadRet.Function;

			return true;
		}

		private static IDictionary<string, object> AnonymousObjectToDict(object obj)
		{
			var properties = obj.GetType ().GetProperties ();
			return properties.ToDictionary (k => k.Name, v => v.GetValue (obj, null));
		}
	}
}

