﻿using System;
using MoonSharp.Interpreter.Loaders;
using MoonSharp.Interpreter;
using System.IO;

namespace aitest.LuaVM
{
	public class PackageScriptLoader : IScriptLoader
	{
		public PackageScriptLoader ()
		{
		}



		#region IScriptLoader implementation
		object IScriptLoader.LoadFile (string file, Table globalContext)
		{
			Console.WriteLine ("READ: " + file);
			return File.OpenRead (file);
		}
		string IScriptLoader.ResolveFileName (string filename, Table globalContext)
		{
			Console.WriteLine ("RESOLVE: " + filename);
			return filename;
		}
		string IScriptLoader.ResolveModuleName (string modname, Table globalContext)
		{
			Console.WriteLine ("MODULE: " + modname);
			return modname.Replace('.', '/') + ".lua";
		}
		#endregion
	}
}

