﻿using System;
using MoonSharp.Interpreter;

namespace aitest.LuaVM
{
	public class LuaScript : ILuaScript
	{
		private readonly Script _script;
		private readonly Closure _entrypoint;

		public LuaScript (Script script, Closure entrypoint)
		{
			_script = script;
			_entrypoint = entrypoint;
		}

		public void Set(string key, object val)
		{
			UserData.RegisterType (val.GetType(), InteropAccessMode.LazyOptimized);
			_script.Globals [key] = val;
		}

		public bool TryGet<T>(string key, out T val)
		{
			var ret = _script.Globals.Get (key);
			if (ret.IsNil())
			{
				val = default(T);
				return false;
			}
			val = ret.ToObject<T> ();
			return true;
		}

		/*private Closure GetFunctionByName(string name)
		{
			if (string.IsNullOrEmpty(name))
			{
				if (_entrypointRet.Type == DataType.Function)
					return _entrypointRet.Function;
				return _entrypoint;
			}

			var func = _script.Globals.Get (name);
			if (func != null && !func.IsNil() && func.Type == DataType.Function)
			{
				return func.Function;
			}

			return null;
		}*/

		/*public ILuaCoroutine CreateCoroutine(string entrypoint = null, params object[] args)
		{
			Closure func = GetFunctionByName (entrypoint);
			if (func == null)
				return null;

			return new LuaCoroutine (func, args);
		}*/

		public ILuaCoroutine CreateEntrypointCoroutine(params object[] args)
		{
			if (_entrypoint == null)
				throw new Exception ("Entrypoint is undefined");
			return new LuaCoroutine (_script.CreateCoroutine(_entrypoint).Coroutine, args);
		}

		public ILuaCoroutine CreateCoroutine(string functionName, params object[] args)
		{
			return new LuaCoroutine (_script.CreateCoroutine (_script.Globals [functionName]).Coroutine, args);
		}

		public TRet RunScript<TRet>(params object[] args)
		{
			if (_entrypoint == null)
				throw new Exception ("Entrypoint is undefined");
			return _entrypoint.Call (args).ToObject<TRet> ();
		}

		public object RunScript(params object[] args)
		{
			return RunScript<object> (args);
		}

		public object Call(string functionName, params object[] args)
		{
			return Call<object>(functionName, args);
		}

		public TRet Call<TRet>(string functionName, params object[] args)
		{
			try
			{
				return _script.Call (_script.Globals [functionName], args).ToObject<TRet> ();
			}
			catch(InterpreterException e)
			{
				Console.Error.WriteLine (e.DecoratedMessage);
			}
			return default(TRet);
		}
	}
}

