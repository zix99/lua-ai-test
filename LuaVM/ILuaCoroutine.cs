﻿using System;

namespace aitest.LuaVM
{
	public interface ILuaCoroutine
	{
		bool Paused{get;}

		void Pause();

		void Resume();

		void Stop();
	}
}

