﻿using System;

namespace aitest.LuaVM
{
	public interface ILuaScript
	{
		void Set(string key, object val);

		ILuaCoroutine CreateEntrypointCoroutine (params object[] args);

		ILuaCoroutine CreateCoroutine(string functionName, params object[] args);
	}
}

