﻿using System;
using MoonSharp.Interpreter;
using aitest.Scripting.VM;

namespace aitest.LuaVM
{
	public class LuaCoroutine : IRunnableCoroutine, ILuaCoroutine
	{
		private readonly Coroutine _coroutine;

		private bool _paused = false;
		private bool _stop = false;
		private int _cycles=0;

		public LuaCoroutine (Coroutine coroutine, params object[] args)
		{
			_coroutine = coroutine;
			_coroutine.AutoYieldCounter = 1;
			_coroutine.Resume (args);
		}

		public static LuaCoroutine CreateCoroutine(Closure Func)
		{
			return new LuaCoroutine (Func.OwnerScript.CreateCoroutine (Func).Coroutine);
		}

		public bool Paused
		{
			get{return _paused;}
		}

		public void Pause()
		{
			_paused = true;
		}

		public void Resume()
		{
			_paused = false;
		}

		public void Stop()
		{
			_stop = true;
		}

		#region IResumableCoroutine implementation

		public long YieldFrequency {
			get {
				return _coroutine.AutoYieldCounter;
			}
			set {
				_coroutine.AutoYieldCounter = value;
			}
		}



		CoroutineResult IRunnableCoroutine.Tick ()
		{
			if (_stop)
				return CoroutineResult.Finished;
			if (_paused)
				return CoroutineResult.Continue;

			try
			{
				_cycles++;
				var ret = _coroutine.Resume();

				if (_coroutine.State == CoroutineState.Dead)
					return CoroutineResult.Finished;
				if (_coroutine.State == CoroutineState.Suspended)
				{
					// Suspended manually
					if (ret.Type == DataType.Nil)
					{
						// do nothing (But not error)
					}
					if (ret.Type == DataType.Number)
					{
						// Do continue for N seconds
						double pauseSecs = ret.Number; //TODO: Implement pause
					}
					else
					{
						// We got a coroutine.yield, but have no idea what it means
						Console.WriteLine("WHAT ARE YOU DOING");
					}
					return CoroutineResult.YieldEarly;
				}
			}
			catch(InterpreterException e)
			{
				Console.WriteLine (e.DecoratedMessage);
				return CoroutineResult.Finished;
			}

			return CoroutineResult.Continue;
		}

		#endregion
	}
}

