﻿using System;
using System.Collections.Generic;

namespace aitest.LuaVM
{
	public interface ILuaScriptManager
	{
		ILuaScript LoadScript(string filename, IDictionary<string, object> globals, params object[] args);

		ILuaScript LoadScript (string filename, object globals, params object[] args);

		ILuaScript LoadScript(string filename);

		ILuaScript PreloadScript(string filename);

		void ExecCoroutine(ILuaCoroutine coroutine);
	}
}

