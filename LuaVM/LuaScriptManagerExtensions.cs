﻿using System;

namespace aitest.LuaVM
{
	public static class LuaScriptManagerExtensions
	{
		public static ILuaCoroutine ExecVM(this ILuaScriptManager scriptManager, ILuaScript script, string functionName, params object[] args)
		{
			var coroutine = script.CreateCoroutine (functionName, args);
			scriptManager.ExecCoroutine (coroutine);
			return coroutine;
		}
	}
}

