﻿using System;

namespace aitest.Scripting.VM
{
	public interface IRunnableCoroutine
	{
		long YieldFrequency{get;set;}

		CoroutineResult Tick();
	}
}

