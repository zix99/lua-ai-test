﻿using System;

namespace aitest.Scripting.VM
{
	public enum CoroutineResult
	{
		Undefined=0,
		Continue,
		YieldEarly,
		Finished
	}
}

