﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace aitest.Scripting.VM
{
	public class CoroutineVM : ICoroutineVM
	{
		private readonly LinkedList<IRunnableCoroutine> _vms = new LinkedList<IRunnableCoroutine> ();
		private LinkedListNode<IRunnableCoroutine> _current;

		private readonly Stopwatch _tickTimer = new Stopwatch ();
		private readonly Stopwatch _vmTimer = new Stopwatch ();

		private TimeSpan _maxTickTime = TimeSpan.FromMilliseconds(2);
		private int _minScripts = 2;
		private int _maxScripts = 25;

		private int _defaultYield = 25;
		private int _minYield = 25;
		private int _maxYield = 1000;
		private int _stepYield = 25;

		public TimeSpan MaxTickTime
		{
			get{return _maxTickTime;}
			set{_maxTickTime = value;}
		}

		public TimeSpan MaxIndividualTickTime
		{
			get
			{
				return TimeSpan.FromTicks(_maxTickTime.Ticks / 6);
			}
		}

		public CoroutineVM ()
		{
		}

		public void AddCoroutine(IRunnableCoroutine vm)
		{
			vm.YieldFrequency = _defaultYield;
			_vms.AddLast (vm);
		}

		public void Tick()
		{
			if (_current == null)
				_current = _vms.First;

			_tickTimer.Restart ();

			int ticks = 0;
			while(_current != null && _tickTimer.Elapsed < _maxTickTime && ticks < _maxScripts)
			{
				_vmTimer.Restart ();
				var ret = _current.Value.Tick ();
				_vmTimer.Stop ();

				// Adjust the yield frequency based on how slow it is
				if (ret != CoroutineResult.YieldEarly)
				{
					if (_vmTimer.Elapsed > this.MaxIndividualTickTime && _current.Value.YieldFrequency > _minYield)
					{
						_current.Value.YieldFrequency = Math.Max (_current.Value.YieldFrequency - _stepYield, _minYield);
						Console.WriteLine ("Scale-down {0}", _current.Value.YieldFrequency);
					}
					if (_vmTimer.Elapsed < this.MaxIndividualTickTime - TimeSpan.FromMilliseconds (0.05) && _current.Value.YieldFrequency < _maxYield)
					{
						_current.Value.YieldFrequency = Math.Min (_current.Value.YieldFrequency + _stepYield, _maxYield);
						Console.WriteLine ("Scale-up {0}", _current.Value.YieldFrequency);
					}
				}

				var curr = _current;
				_current = _current.Next;

				if (ret == CoroutineResult.Finished)
					_vms.Remove (curr);

				++ticks;
			}

			_tickTimer.Stop ();

			if (_tickTimer.Elapsed.TotalMilliseconds > this.MaxTickTime.TotalMilliseconds*1.25)
				Console.WriteLine ("TOO MUCH TICK TIME: {0}", _tickTimer.Elapsed.TotalMilliseconds);
		}
	}
}

