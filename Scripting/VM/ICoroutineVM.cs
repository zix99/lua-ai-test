﻿using System;

namespace aitest.Scripting.VM
{
	public interface ICoroutineVM
	{
		void AddCoroutine(IRunnableCoroutine vm);
	}
}

