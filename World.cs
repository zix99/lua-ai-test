﻿using System;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Collections.Generic;
using aitest.Objects;

namespace aitest
{
	public class World
	{
		public readonly int Width, Height;

		private readonly List<WorldObject> _objects = new List<WorldObject>();

		public World(int width, int height)
		{
			this.Width = width;
			this.Height = height;
		}

		public void AddObject(WorldObject obj)
		{
			_objects.Add (obj);
		}

		public void RemoveObject(WorldObject obj)
		{
			_objects.Remove (obj);
		}

		public bool CanMove(Vector2 pos)
		{
			if (pos.X < 0 || pos.Y < 0 || pos.X > this.Width || pos.Y > this.Height)
				return false;

			return true;
		}

		public WorldObject FindObjectNear(Vector2 pos)
		{
			return _objects
				.OrderBy (x => (x.Position - pos).LengthFast)
				.FirstOrDefault ();
		}

		public void Render()
		{
			GL.Color3 (0.2f, 0.1f, 0f);
			GL.Begin (PrimitiveType.Quads);
			GL.Vertex2 (0, 0);
			GL.Vertex2 (this.Width, 0);
			GL.Vertex2 (this.Width, this.Height);
			GL.Vertex2 (0, this.Height);
			GL.End ();

			GL.Enable (EnableCap.Blend);
			GL.BlendFunc (BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

			for(int i=0; i<_objects.Count; ++i)
			{
				_objects [i].Render ();
			}

			GL.Disable (EnableCap.Blend);
		}
	}
}

