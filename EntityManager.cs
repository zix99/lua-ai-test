﻿using System;
using System.Collections.Generic;
using aitest.Entities;

namespace aitest
{
	public class EntityManager
	{
		private readonly List<Entity> _entities = new List<Entity>();

		public IEnumerable<Entity> Entities
		{
			get{return _entities;}
		}

		public EntityManager ()
		{
		}

		public void AddEntity(Entity entity)
		{
			_entities.Add (entity);
		}

		public void Simulate()
		{
			for (int i=_entities.Count-1; i>=0; --i)
			{
				_entities [i].Simulate ();
				if (_entities[i].Destroyable)
				{
					_entities [i].Dispose ();
					_entities.RemoveAt (i);
				}
			}
		}

		public void Render()
		{
			for (int i=0; i<_entities.Count; ++i)
			{
				_entities [i].Render ();
			}
		}
	}
}

