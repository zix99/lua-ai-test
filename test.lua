﻿-- require './req.lua'

-- dofile("req.lua")

if not bla then
	print(bla)
end

calls = 0
superprint("abc")

function doThing()
	print("abc", calls)
	calls = calls + 1
	print(string.format([[You were called: %d times]], calls))
	local s = "ABC you were called %d times"
	print(s:format(calls))
	print(#"abcasdfasdf")
end

function think()
	print(entity)
	superprint(entity.Position)
end

function whenRandom(chance)
	return math.random(100) <= chance
end

function idle()
end

function attack()
	entity.attackNear(enemy)
end
globalConnect(whenNearEnemy, attack)

function boredWander()
	entity.moveTo(5,5)
end
connect(idle, whenRandom(10), boredWander)

function boredJump()
	entity.jump()
end
connect(idle, whenRandom(10), boredJump)
