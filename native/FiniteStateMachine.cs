using System;
using System.Collections.Generic;
using System.Reflection;

namespace aitest.native
{
    public delegate bool FSMCondition();
    public delegate FSMCondition FSMNode();
    //public delegate float FSMInfluence();

    public abstract class FiniteStateMachine : IBrain
    {
        protected static readonly TimeSpan DEFAULT_TIMEOUT = TimeSpan.FromSeconds(30);

        private class Connection
        {
            public FSMCondition Condition;
            public FSMNode Target;
            public TimeSpan Timeout;
            public FSMNode[] Blacklist = new FSMNode[0];
            public bool CanSelfReference = true;
            public bool Interruptive;
        }

        [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
        protected class FSMNodeAttribute : Attribute
        { }

        private static int _seed = (int)(DateTime.UtcNow.Ticks % int.MaxValue);
        protected readonly Random Random = new Random(_seed++);

        private readonly Dictionary<FSMNode, List<Connection>> _connections = new Dictionary<FSMNode, List<Connection>>();
        private readonly List<Connection> _globalConnections = new List<Connection>();

        private FSMNode _currentNode;
        private FSMCondition _currentCondition;
        private DateTime _currentTimeout;

        protected FiniteStateMachine ()
        {
            this.Init ();
        }

        protected abstract void Init();

        [FSMNode]
        protected abstract FSMCondition Idle();

        protected abstract void Tick ();

        protected void Connect(FSMNode fromNode, FSMCondition when, FSMNode toNode, TimeSpan? timeout = null)
        {
            List<Connection> connections;
            if (!_connections.TryGetValue(fromNode, out connections))
            {
                _connections [fromNode] = connections = new List<Connection> ();
            }

            connections.Add(new Connection{
                Condition = when,
                Target = toNode,
                Timeout = timeout ?? DEFAULT_TIMEOUT
            });
        }

        protected void ConnectGlobal(FSMCondition when, FSMNode toNode, FSMNode[] blacklist = null, TimeSpan? timeout = null)
        {
            _globalConnections.Add(new Connection{
                Condition = when,
                Target = toNode,
                Timeout = timeout ?? DEFAULT_TIMEOUT,
                Blacklist = blacklist,
                CanSelfReference = false
            });
        }

        protected FSMCondition WhenRandom(float likelihood)
        {
            return () => Random.NextDouble() < likelihood;
        }

        protected FSMCondition WhenWait(TimeSpan time)
        {
            var target = DateTime.UtcNow + time;
            return () => DateTime.UtcNow >= target;
        }

        #region IBrain implementation
        void IBrain.Init ()
        {
            this.Init ();
        }
        void IBrain.Destroy ()
        {

        }
        void IBrain.Think ()
        {
            this.Tick ();

            if (_currentCondition != null && !_currentCondition ())
                return;

            List<Connection> connections;
            FSMNode targetNode = null;
            if (_currentNode != null && _connections.TryGetValue (_currentNode, out connections))
                targetNode = TryFindNode (connections);

            if (targetNode == null) // Try global connections
                targetNode = TryFindNode (_globalConnections);

            if (targetNode != null)
                SetNode(targetNode);
            else
                SetNode(this.Idle);
        }

        private FSMNode TryFindNode(IList<Connection> connections)
        {
            for (int i=0; i<connections.Count; ++i)
            {
                var conn = connections [i];
                if ( (conn.CanSelfReference || conn.Target != _currentNode)
                    && !Array.Exists(conn.Blacklist, x => x == _currentNode)
                    && conn.Condition())
                {
                    return conn.Target;
                }
            }
            return null;
        }

        private void SetNode(FSMNode node)
        {
            Console.WriteLine ("{0}: Set to node {1}", this, node != null ? node.Method.Name : "Null");
            _currentNode = node;
            _currentCondition = node () ?? (() => true);
        }

        #endregion
    }
}

