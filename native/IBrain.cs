using System;

namespace aitest.native
{
    public interface IBrain
    {
        void Init();
        void Destroy();
        void Think();
    }
}

