﻿using System;
using aitest.Objects;
using OpenTK;
using aitest.Entities;
using aitest.LuaVM;
using MoonSharp.Interpreter;
using aitest.Scripting.VM;

namespace aitest
{
	public class Game
	{
		private readonly Random _random = new Random();

		public readonly World World = new World(1000, 1000);
		public readonly EntityManager Entities = new EntityManager ();

		private readonly CoroutineVM _vm;
		public readonly ILuaScriptManager ScriptManager;

		private Game()
		{
			_vm = new CoroutineVM ();
			ScriptManager = new LuaScriptManager (_vm);
		}

		public void Initialize()
		{
			UserData.RegisterType<Vector2> ();
			UserData.RegisterType<WorldObject> ();

			for (int i=0; i<1000; ++i)
			{
				var pt = new Vector2 (_random.Next (World.Width), _random.Next (World.Height));
				this.World.AddObject (new Tree (pt));
			}
				
			for (int i=0; i<10; ++i)
			{
				this.Entities.AddEntity (new Ant ());
			}
		}

		[ThreadStatic]
		private static Game _instance;
		public static Game Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Game ();
					_instance.Initialize ();
				}
				return _instance;
			}
		}

		public void Simulate()
		{
			_vm.Tick ();
			this.Entities.Simulate ();
		}

		public void Render()
		{
			this.World.Render ();
			this.Entities.Render ();
		}
	}
}

