﻿using System;
using aitest.LuaVM;
using aitest.native;

namespace aitest.Entities
{
	public abstract class BrainEntity : Entity
	{
		private readonly ILuaScript _brain;
		private readonly ILuaCoroutine _brainVM;

        private readonly IBrain _brain2;

		public BrainEntity ()
		{
			/*_brain = Game.Instance.ScriptManager.LoadScript (this.BrainFilename, new{
				entity = this,
				world = Game.Instance.World
			});
			_brainVM = Game.Instance.ScriptManager.ExecVM (_brain, "main");*/

            _brain2 = new NativeAntBrain (this);
		}

		protected abstract string BrainFilename{get;}

		public override void Dispose ()
		{
			base.Dispose ();
			//_brainVM.Stop ();
		}

        public override void Simulate ()
        {
            base.Simulate ();
            _brain2.Think ();
        }
	}
}

