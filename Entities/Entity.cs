﻿using System;
using System.Linq;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace aitest.Entities
{
	public abstract class Entity : IDisposable
	{
		private static readonly Random _random = new Random();

		public Vector2 Position{ get; protected set;}

		public Color4 Color{ get; protected set;}

		public Vector2? TargetPosition{ get; protected set;}

		public float Speed = 1.0f;

		public float DrawSize
		{
			get
			{
				return 1f + this.Age * 5f;
			}
		}

		public float Age = 0f;
		public float MaxAge = 1f;
		public float Species = 0.5f;
		public int Sex = 0;

		public bool Destroyable{ get; private set;}

		public Entity ()
		{
			this.Color = new Color4 (1f, 1f, 1f, 1f);
			this.Sex = _random.Next (0, 2);
			this.MaxAge = (float)_random.NextDouble () * 0.2f + 1f;
		}

		public virtual void Dispose(){}

		public virtual void Simulate()
		{
			if (this.TargetPosition.HasValue)
			{
				Vector2 dir = (this.TargetPosition.Value - this.Position).Normalized ();
				var newPos = this.Position + dir * this.Speed;

				if (Game.Instance.World.CanMove(newPos))
				{
					this.Position = newPos;
					if ((this.TargetPosition.Value - this.Position).Length < 1f)
						this.TargetPosition = null;
				}
				else
				{
					this.TargetPosition = null;
				}
			}

			this.Age += 0.001f;
			if (this.Age > this.MaxAge)
				this.Die ();
		}

		public void Render()
		{
			GL.Color4 (new Color4(this.Color.R + this.Species, this.Color.G, this.Sex, this.Color.A));
			GL.Begin (PrimitiveType.Quads);
			GL.Vertex2 (this.Position.X - this.DrawSize, this.Position.Y - this.DrawSize);
			GL.Vertex2 (this.Position.X + this.DrawSize, this.Position.Y - this.DrawSize);
			GL.Vertex2 (this.Position.X + this.DrawSize, this.Position.Y + this.DrawSize);
			GL.Vertex2 (this.Position.X - this.DrawSize, this.Position.Y + this.DrawSize);
			GL.End ();
		}


		#region helper properties

		public bool IsMoving
		{
			get{return this.TargetPosition.HasValue;}
		}

		#endregion

		#region helpers methods

		public void Wander(int dist)
		{
			this.TargetPosition = this.Position + new Vector2 (_random.Next (-dist, dist), _random.Next (-dist, dist));
		}

		public void MoveTo(Vector2 pos)
		{
			this.TargetPosition = pos;
		}

		public void Die()
		{
			this.Destroyable = true;
		}

		public void Mate(Entity with)
		{
			if (this.Sex != with.Sex) {
				var ant = new Ant ();
				ant.Position = this.Position;
				Game.Instance.Entities.AddEntity (ant);
			}
		}

		public Entity FindOppositeSex()
		{
			foreach(var entity in Game.Instance.Entities.Entities)
			{
				if (entity.Sex != this.Sex)
					return entity;
			}
			return null;
		}

		#endregion
	}
}

