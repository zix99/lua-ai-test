using System;
using aitest.native;
using OpenTK;
using aitest.Objects;

namespace aitest.Entities
{
    public class NativeAntBrain : FiniteStateMachine
    {
        private readonly Entity _entity;

        private float _hunger = 0f;

        public NativeAntBrain (Entity entity)
        {
            _entity = entity;
        }

        protected FSMCondition Wander()
        {
            var newPos = _entity.Position + new Vector2 (Random.Next (-50, 50), Random.Next (-50, 50));
            _entity.MoveTo (newPos);
            return () => !_entity.IsMoving;
        }

        private WorldObject _targetFood;

        [FSMNode]
        protected FSMCondition Hungry()
        {
            _targetFood = Game.Instance.World.FindObjectNear (_entity.Position);
            _entity.MoveTo (_targetFood.Position);
            return () => !_entity.IsMoving;
        }

        [FSMNode]
        protected FSMCondition Eat()
        {
            Console.WriteLine ("EAT");
            Game.Instance.World.RemoveObject (_targetFood);
            _targetFood = null;
            _hunger -= 0.5f;
            return WhenWait (TimeSpan.FromSeconds(0.5));
        }

        #region implemented abstract members of FiniteStateMachine

        protected override void Init ()
        {
            this.Connect (Idle, WhenRandom (0.5f), Wander);

            this.ConnectGlobal (() => _hunger > 0.5f, this.Hungry, new FSMNode[]{this.Eat});
            this.Connect (this.Hungry, () => true, this.Eat);
        }

        protected override FSMCondition Idle ()
        {
            return this.WhenWait (TimeSpan.FromSeconds (2));
        }

        protected override void Tick ()
        {
            _hunger += 0.005f;
        }

        #endregion

        public override string ToString ()
        {
            return string.Format ("[{0}]", _entity);
        }
    }
}

