﻿using System;
using OpenTK.Graphics;
using System.IO;
using OpenTK;
using MoonSharp.Interpreter;

namespace aitest.Entities
{
	public class Ant : BrainEntity
	{
		private static readonly Random _random = new Random();

		public Ant ()
		{
			this.Color = new Color4 (1f, 0f, 0f, 1f);
			this.Position = new Vector2(_random.Next(Game.Instance.World.Width), _random.Next(Game.Instance.World.Height));
		}

		protected override string BrainFilename {
			get {
				return "Entities/antai.lua";
			}
		}
	}
}

