﻿local on_idle = nil
local on_tick = nil
local active_node = nil
local connections = {}
local globalConnections = {}

local clock = os.clock
function sleep(secs)
	local t0 = clock()
	while clock() - t0 <= secs do
		coroutine.yield(0.1)
	end
end

local function evaluateConnections(connections)
	for idx, conn in ipairs(connections) do
		if not conn.blacklist[active_node] and active_node ~= conn.dst then

			local success, ret = pcall(conn.when)
			if success and ret then
				active_node = conn.dst
				if active_node then pcall(active_node) end
				break
			elseif not success then
				print("Error evaluating", conn, ret)
			end

		end
	end
end

local function runNodes()
	if not active_node then
		active_node = on_idle
		active_node()
	else
		if connections[active_node] then
			evaluateConnections(connections[active_node].targets)
		end
	end

	-- Global transitions
	evaluateConnections(globalConnections)
end

function main()
	while true do
		local success, ret = pcall(runNodes)
		if not success then print(ret) end

		-- tick
		if on_tick then on_tick() end

		sleep(0.2)
	end
end

function setIdle(node0)
	on_idle = node0
end

function setTick(tick)
	on_tick = tick
end

function connect(node0, when, node1)
	if not connections[node0] then
		connections[node0] = { targets = {} }
	end
	table.insert(connections[node0].targets, {when = when, dst = node1, blacklist = {}})
end

function connectGlobal(when, node1, blacklist)
	table.insert(globalConnections, {when = when, dst = node1, blacklist = blacklist or {}})
end