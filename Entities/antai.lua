﻿require "Entities.fsm"

-------------------------------- BRAIN VARS

hunger = 0
mate_cooloff = 0

function tick()
	hunger = hunger + 0.01
	mate_cooloff = math.max(0, mate_cooloff - 0.01)
end
setTick(tick)

-------------------------------- BEGIN BRAIN

function whenRandom(chance)
	return function() return math.random(100) < chance end
end

function whenDoneMoving()
	return not entity.IsMoving
end

function whenHungry()
	return hunger > 0.7
end

local clock = os.clock
function whenWait(secs)
	local target = clock() + secs
	return function() return clock() >= secs end
end

function whenAnd(a, b)
	return function()
		return a() and b()
	end
end

function idle()
	print "IDLE NODE"
end
setIdle(idle)

function boredWander()
	print "Wandering"
	entity.Wander(100)
end
connect(idle, whenRandom(1.5), boredWander)
connect(boredWander, whenDoneMoving, idle)

eat_target = nil
function eat()
	if eat_target then
		world.RemoveObject(eat_target)
		eat_target = nil
		hunger = math.max(0, hunger - (math.random(1.0)+0.2))
		print(string.format("EATING %f", hunger))
	end
end
connect(eat, whenWait(1), idle)

function imHungry()
	print "NEED FOOD"
	local food = world.FindObjectNear(entity.Position)
	if food then
		entity.MoveTo(food.Position)
		eat_target = food
	end
end
connectGlobal(whenHungry, imHungry, {imHungry, eat})
connect(imHungry, whenDoneMoving, eat)

-- Mating

target_mate = nil

function whenMatable()
	return entity.Age > 0.05 and mate_cooloff < 0.01
end

function whenNearMate()
	if not target_mate then return false end
	return (entity.Position - target_mate.Position).Length < 5
end

function whenTargetingMate()
	return target_mate ~= nil
end

function findMate()
	print("Find Mate")
	local mate = entity.FindOppositeSex()
	if mate then
		target_mate = mate
		entity.MoveTo(mate.Position)
	end
end

function moveToMate()
	print("Re-finding mate")
	entity.MoveTo(target_mate.Position)
end

function doMate()
	print("MATE", entity, target_mate)
	entity.Mate(target_mate)
	target_mate = nil
	mate_cooloff = mate_cooloff + 1
end

connect(idle, whenAnd(whenRandom(20), whenMatable), findMate)
connect(findMate, whenNearMate, doMate)
connect(findMate, whenAnd(whenDoneMoving, whenTargetingMate), moveToMate)
connect(doMate, whenWait(0.5), idle)

