﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System.Collections.Generic;
using aitest.Entities;
using aitest.LuaVM;

namespace aitest
{
	public class MainWindow : GameWindow
	{
		public MainWindow ()
			:base(1440, 900)
		{
		}

		protected override void OnLoad (EventArgs e)
		{
            base.OnLoad (e);
			GL.ClearColor (0, 0, 0, 0);
			this.TargetUpdateFrequency = 15;
			this.TargetRenderFrequency = 30;
			this.Title = "AI Test";
		}

		protected override void OnRenderFrame (FrameEventArgs e)
		{
            base.OnRenderFrame (e);
			GL.Clear (ClearBufferMask.ColorBufferBit);

			GL.MatrixMode (MatrixMode.Projection);
			GL.LoadIdentity ();
			GL.MatrixMode (MatrixMode.Modelview);
			var ortho = Matrix4d.CreateOrthographicOffCenter (0, Game.Instance.World.Width, Game.Instance.World.Height, 0, 0, 10);
			GL.LoadMatrix (ref ortho);

			Game.Instance.Render ();

			this.SwapBuffers ();
		}

		protected override void OnUpdateFrame (FrameEventArgs e)
		{
            base.OnUpdateFrame (e);
			Game.Instance.Simulate ();
		}

		protected override void OnKeyDown (KeyboardKeyEventArgs e)
		{
            base.OnKeyDown (e);
			if (e.Keyboard.IsKeyDown (Key.Escape))
				this.Exit ();
		}

		protected override void OnResize (EventArgs e)
		{
            base.OnResize (e);
			GL.Viewport (0, 0, this.Width, this.Height);
		}
	}
}

