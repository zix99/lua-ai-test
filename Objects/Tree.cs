﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;

namespace aitest.Objects
{
	public class Tree : WorldObject
	{
		private static readonly Random _random = new Random();

		public int Size = 10;

		public Color4 Color = new Color4(0f, 0.4f + (float)(_random.NextDouble()*0.2f), 0f, 1f);

		public Tree (Vector2 pos)
			:base(pos)
		{
		}

		public override void Render ()
		{
			GL.Color4 (this.Color);
			GL.Begin (PrimitiveType.Quads);
			GL.Vertex2 (this.Position.X - this.Size, this.Position.Y - this.Size);
			GL.Vertex2 (this.Position.X + this.Size, this.Position.Y - this.Size);
			GL.Vertex2 (this.Position.X + this.Size, this.Position.Y + this.Size);
			GL.Vertex2 (this.Position.X - this.Size, this.Position.Y + this.Size);
			GL.End ();
		}
	}
}

