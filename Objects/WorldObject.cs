﻿using System;
using OpenTK;

namespace aitest.Objects
{
	public abstract class WorldObject
	{
		public readonly Vector2 Position;

		public WorldObject (Vector2 pos)
		{
			this.Position = pos;
		}

		public abstract void Render();
	}
}

