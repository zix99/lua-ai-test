using System;
using System.Collections.Generic;
using System.Text;

namespace lunar
{
    public static class InputHelper
    {
        private static readonly List<string> _buffer = new List<string>();

        public static int TabWidth = 2;
        public static bool AutoIndent = true;

        public static string ReadLine(string prompt = "")
        {
            int bufferIdx = _buffer.Count;

            var lineBuffer = new StringBuilder ();
            var multilineBuffer = new List<string> ();
            bool multiline = false;

            int lineOffset = 0;
            int prevRenderedLength = 0;

            Console.Write (prompt);

            while(true)
            {
                var key = Console.ReadKey (true);

                if (key.Modifiers.HasFlag(ConsoleModifiers.Control) && key.Key == ConsoleKey.D)
                { // Input terminator
                    return null;
                }
                else if (key.Key == ConsoleKey.UpArrow && !multiline)
                {
                    bufferIdx = Math.Max (0, bufferIdx - 1);
                    lineBuffer.Clear ();
                    lineBuffer.Append (_buffer [bufferIdx]);
                }
                else if (key.Key == ConsoleKey.DownArrow && !multiline)
                {
                    bufferIdx = Math.Min (_buffer.Count, bufferIdx + 1);
                    if (bufferIdx < _buffer.Count)
                    {
                        lineBuffer.Clear ();
                        lineBuffer.Append (_buffer [bufferIdx]);
                    }
                    else
                    {
                        lineBuffer.Clear ();
                    }
                }
                else if (key.Key == ConsoleKey.LeftArrow)
                {
                    lineOffset = Math.Min(lineBuffer.Length, lineOffset + 1);
                }
                else if (key.Key == ConsoleKey.RightArrow)
                {
                    lineOffset = Math.Max (0, lineOffset - 1);
                }
                else if (key.Key == ConsoleKey.Tab)
                {
                    lineBuffer.Insert (lineBuffer.Length - lineOffset, new String(' ', TabWidth));
                }
                else if (key.Key == ConsoleKey.Backspace)
                {
                    int idx = lineBuffer.Length - lineOffset - 1;
                    if (idx >= 0 && idx < lineBuffer.Length)
                        lineBuffer.Remove (idx, 1);
                }
                else if (key.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine ();

                    if (lineBuffer.Length > 0 && lineBuffer[lineBuffer.Length-1] == '\\') // begin multiline
                    {
                        multiline = true;
                        lineBuffer.Remove (lineBuffer.Length - 1, 1); //Remove the \
                    }

                    string line = lineBuffer.ToString ();
                    multilineBuffer.Add (line);
                    lineBuffer.Clear ();
                    lineOffset = 0;

                    if (!multiline || (string.IsNullOrEmpty(multilineBuffer[multilineBuffer.Count-1]) && multilineBuffer.Count > 1) )
                    {
                        string ret = string.Join ("\n", multilineBuffer);
                        if (!string.IsNullOrWhiteSpace (ret))
                            _buffer.Add (ret.Replace('\n', ' '));
                        return ret;
                    }

                    if (AutoIndent)
                    {
                        // re-indent next line to match previous
                        int spaceCount = 0;
                        for (; spaceCount < line.Length && line[spaceCount] == ' '; ++spaceCount){}
                        int tabInsert = spaceCount / TabWidth;
                        if (tabInsert > 0)
                            lineBuffer.Append (new String (' ', TabWidth * tabInsert));
                    }
                }
                else if (key.KeyChar != 0)
                {
                    lineBuffer.Insert (lineBuffer.Length - lineOffset, key.KeyChar);
                }

                // Redraw
                string rendered = string.Format ((multiline ? string.Empty : prompt) + lineBuffer);
                Console.CursorLeft = 0;
                Console.Write (rendered);
                for (int i=0; i<prevRenderedLength - rendered.Length; ++i)
                    Console.Write (' '); // If last line was longer, fill in with whitespace
                Console.CursorLeft = rendered.Length - lineOffset;
                Console.Out.Flush ();
                prevRenderedLength = rendered.Length;
            }
        }

    }
}

