﻿using System;
using MoonSharp.Interpreter;
using System.IO;

namespace lunar
{
	class MainClass
	{
		public static void Main (string[] args)
		{
            Script script = new Script ();

            Console.WriteLine ("Moonsharp REPL");
            Console.WriteLine ("  Version: " + typeof(Script).Assembly.GetName ().Version.ToString ());
            Console.WriteLine ("  Type /help to view commands");

            while(true)
            {
                try
                {
                    string input = InputHelper.ReadLine(">>> ");
                    if (input == null)
                        break;

                    if (input.StartsWith("/"))
                    {
                        string[] parts = input.Split();
                        switch(parts[0])
                        {
						case "/help":
							Console.WriteLine("Available Commands:");
							Console.WriteLine("  /exit          Exits the session");
							Console.WriteLine("  /val <global>  Displays value of a given global");
							Console.WriteLine("  /vals          Output all global values");
							break;
                        case "/exit":
                            Environment.Exit(0);
                            break;
                        case "/val":
                            var val = script.Globals.Get(parts[1]);
                            Console.WriteLine(val);
                            break;
                        case "/vals":
                            foreach(var pair in script.Globals.Pairs)
                            {
                                Console.WriteLine("{0} = {1}", pair.Key, pair.Value);
                            }
                            break;
                        default:
                            Console.Error.WriteLine("Unknown command: {0}\n  Type /help", input);
                            break;
                        }
                    }
                    else
                    {
                        var ret = script.DoString(input);

                        if (!ret.IsVoid())
                            Console.WriteLine(ret);
                    }
                }
                catch(InterpreterException e)
                {
                    Console.Error.WriteLine (e.DecoratedMessage);
                }
                catch(Exception e)
                {
                    Console.Error.WriteLine (e.Message);
                }
            }
		}
	}
}
